package com.letgo.worksohp.constraintlayout.constraintlayoutworkshop

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.transition.TransitionManager
import android.view.View
import kotlinx.android.synthetic.main.activity_04_demo.*

class ActivityDemo04 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_04_demo)

        fun swap(view: View) {
            TransitionManager.beginDelayedTransition(constraintLayout)
            placeHolder1.setContentId(view.id)
        }

        val views = listOf(imageView1, imageView2, imageView3, imageView2, imageView4)
        views.forEach { it.setOnClickListener { swap(it) } }
    }
}

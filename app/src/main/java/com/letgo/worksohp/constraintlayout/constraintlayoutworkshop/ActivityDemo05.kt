package com.letgo.worksohp.constraintlayout.constraintlayoutworkshop

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.transition.TransitionManager
import android.view.View.GONE
import android.view.View.VISIBLE
import kotlinx.android.synthetic.main.activity_05_demo.*

class ActivityDemo05 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_05_demo)

        button.setOnClickListener {
            TransitionManager.beginDelayedTransition(constraintLayout)

            textTitle.visibility = if (textTitle.visibility == VISIBLE) GONE else VISIBLE
            textSubTitle.visibility = if (textSubTitle.visibility == VISIBLE) GONE else VISIBLE

//            titleGroup.visibility = if (titleGroup.visibility == VISIBLE) GONE else VISIBLE
        }
    }
}

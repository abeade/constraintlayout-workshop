package com.letgo.worksohp.constraintlayout.constraintlayoutworkshop

import android.os.Bundle
import android.support.v7.app.AppCompatActivity

class ActivityDemo01 : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_01_demo)
    }
}

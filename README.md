# Constraint Layout Workshop

![ConstraintLayout Sketch](/images/constraint-layout-sketch.jpeg =250x)

## Constraint layout

> ConstraintLayout allows you to create large and complex layouts with a flat view hierarchy (no nested view groups). It's similar to RelativeLayout in that all views are laid out according to relationships between sibling views and the parent layout, but it's more flexible than RelativeLayout and easier to use with Android Studio's Layout Editor.

[Android Developers Documentation](https://developer.android.com/training/constraint-layout/)

## Project contents

This project cotains sample layouts from letgo app to be refactored using contraint layout

package com.letgo.worksohp.constraintlayout.constraintlayoutworkshop

import android.os.Bundle
import android.support.constraint.ConstraintSet
import android.support.v7.app.AppCompatActivity
import android.transition.TransitionManager
import kotlinx.android.synthetic.main.activity_04_demo.*

class ActivityDemo10 : AppCompatActivity() {

    private var constraintSet1: ConstraintSet = ConstraintSet()
    private var constraintSet2: ConstraintSet = ConstraintSet()
    private var isOriginal = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_04_demo)

        constraintSet1.clone(constraintLayout)
        constraintSet2.clone(this, R.layout.activity_10_demo)

        fun toggle() {
            TransitionManager.beginDelayedTransition(constraintLayout)
            if (isOriginal) {
                constraintSet2.applyTo(constraintLayout)
            } else {
                constraintSet1.applyTo(constraintLayout)
            }
            isOriginal = !isOriginal
        }

        val views = listOf(imageView1, imageView2, imageView3, imageView2, imageView4)
        views.forEach { it.setOnClickListener { toggle() } }
    }
}
